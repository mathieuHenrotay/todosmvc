<?php
/*
    ./app/routeurs/tasksRouteur.php
    Routeur des Tasks
 */

include_once '../app/controleurs/tasksControleur.php';
use \App\Controleurs\Tasks;

  switch ($_GET['tasks']):
    case 'add':
      // ROUTE D' AJOUT D'UNE TACHE
      // PATTERN: tasks/add
      // CTRL:tasksControleur
      // ACTION: Action

      \App\Controleurs\Tasks\addAction($connexion, $_POST['content']);
      break;
    case 'delete':
      // ROUTE DE SUPPRESION D'UNE TACHE
      // PATTERN: tasks/delete/id
      // CTRL:tasksControleur
      // ACTION: deleteAction

      \App\Controleurs\Tasks\deleteAction($connexion, $_GET['id']);
      break;
    case 'toggleFinish':
      // ROUTE DE TRANSFORMATION D'UNE TACHE EN TERMINEE OU NON
      // PATTERN: tasks/toggleFinish/id
      // CTRL:tasksControleur
      // ACTION: toggleFinishAction

      \App\Controleurs\Tasks\toggleFinishAction($connexion, [
        'finished'=>intval($_POST['finished']),
        'id'=>$_GET['id']
      ]);
      break;
    case 'edit':
      // ROUTE DE MODIFICATION D'UNE TACHE
      // PATTERN: tasks/edit/id
      // CTRL:tasksControleur
      // ACTION: editAction

      \App\Controleurs\Tasks\editAction($connexion, [
        'content'=>$_POST['content'],
        'id'=>$_GET['id']
      ]);
      break;
    case 'deleteFinished':
      // ROUTE DE SUPPRESSION DES TERMINES
      // PATTERN: tasks/deleteFinished
      // CTRL:tasksControleur
      // ACTION: Action

      \App\Controleurs\Tasks\deleteFinishedAction($connexion);
      break;
  endswitch;
