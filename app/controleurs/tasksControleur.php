<?php
/*
    ./app/controleurs/pagesControleur.php
    Contrôleur des pages
 */
    namespace App\Controleurs\Tasks;
    use App\Modeles\Task;

    function indexAction(\PDO $connexion, array $params = []){
      include_once '../app/modeles/tasksModele.php';
      $tasks = Task\findAll($connexion, $params);
      $nbreTasks = Task\findFinishedCount($connexion);

      GLOBAL $title, $content1;
      $title = TITRE_DEFAUT;
      ob_start();
        include '../app/vues/tasks/index.php';
      $content1 = ob_get_clean();
    }

    function addAction(\PDO $connexion, STRING $content=null){
      include_once '../app/modeles/tasksModele.php';
      //j'insère une tâche et je récupère son id
      $id=Task\insert($connexion, $content);
      //je récupère le détail d'une tache
      $task=Task\findOneById($connexion, $id);
      //j'inclus la vue task pour la renvoyer au script
      include '../app/vues/tasks/task.php';
    }

    function editAction(\PDO $connexion, ARRAY $data){
      include_once '../app/modeles/tasksModele.php';
      //je mets à jour une tâch et j'envois au script si ça s'est bien passé
      echo Task\updateOneById($connexion, $data);
    }

    function toggleFinishAction(\PDO $connexion, ARRAY $data=null){
      include_once '../app/modeles/tasksModele.php';
      //je mets à jour la colonne finished et je dis au script si ça s'est bien passé
      echo Task\updateFinishedOneById($connexion, $data);
    }

    function deleteAction(\PDO $connexion, INT $id=null){
      include_once '../app/modeles/tasksModele.php';
      //je supprime une tâche à partir de son id et je dis au script si ça s'est bien passé
      echo Task\deleteOneById($connexion, $id);
    }

    function deleteFinishedAction(\PDO $connexion){
      include_once '../app/modeles/tasksModele.php';
      // je supprime dans la db toutes les taches terminées et je dis au script si ça s'est bien passé
      echo Task\deleteAllFinished($connexion);
    }
