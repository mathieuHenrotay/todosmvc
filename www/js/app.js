/*
  ./www/js/app.js
*/
$(function(){
  /*------------------------------FONCTION QUI COMPTE LE NOMBRE DE TACHES ET QUI ACTUALISE LE FOOTER------------------------------*/
  function compteurTaches(){
    //je compte le nombre de taches non terminées
    var active=$('.todo-list li:not(.completed)').length;
    //je compte le nombre de taches terminées
    var finished=$('.todo-list li.completed').length;
    //je modifie le nbre de taches actives dans le footer
    $('.todo-count strong').text(active);
    //j'affiche ou pas la possibilité de supprimer des taches terminées
    if (finished==0) {
      $('.clear-completed').hide();
    }
    else{
      $('.clear-completed').show();
    }
  }
  //je Lance une première fois la fonction pour écraser les valeurs envoyées par le backend
  compteurTaches();

  /*--------------------------------------------AJOUT D'UNE TACHE DEPUIS L'INPUT--------------------------------------------*/
  // AJAX - AJOUT D'UNE TACHE
    //PATTERN: tasks/add
    //DONNEES: -content:valeur de l'input
    //RETURN: la vue task (details d'une tache)
  $('.new-todo').change(function(){
    $.ajax({
        url: 'tasks/add',
        method: 'post',
        data: {
          content:$(this).val()
        },
        success: (reponsePHP)=>{
          $('.todo-list').prepend(reponsePHP).find('.todo:first-of-type').hide().slideDown(()=>{
            $(this).val('').blur();
          });
          compteurTaches();
        },
        error: function(){
          window.alert("Problème durant la transaction...");
        }
    });
  });
  /*--------------------------------------------FIN AJOUT D'UNE TACHE DEPUIS L'INPUT--------------------------------------------*/

  /*--------------------------------------------SUPPRESSION D'UNE TACHE AVEC LA CROIX--------------------------------------------*/
  $('.todo-list').on('click', '.destroy', function(){
    var id=$(this).closest('li').attr('data-id');
    // AJAX - SUPPRESSION D'UNE TACHE
      //PATTERN:tasks/delete/id
      //DONNEES:/
      //RETURN:0(mal passé) ou 1(bien passé)
    $.ajax({
        url: 'tasks/delete/'+id,
        success: (reponsePHP)=>{
          if (reponsePHP!=0) {
            $(this).closest('li').slideUp(function(){
              $(this).remove();
              compteurTaches();
            });
          }
        },
        error: function(){
          window.alert("Problème durant la transaction...");
        }
    });
  });
  /*-----------------------------------------FIN SUPPRESSION D'UNE TACHE AVEC LA CROIX-----------------------------------------*/

  /*--------------------------------------POSSIBILITE DE COCHER UNE TACHE POUR LA TERMINER--------------------------------------*/

  $('.todo-list').on('change', '.toggle', function(){
    var toggle=$(this).prop('checked');
    //je suis obligé de faire une condition pour transformer le true ou false de la propriété checked en 0 ou 1(ni intval coté serveur, ni parseInt coté client ne fonctionnent)
    if (toggle==true) {
      toggle=1;
    }
    else{
      toggle=0;
    }
    var id=$(this).closest('li').attr('data-id');
    // AJAX - MODIFICATION DE LA COLONNE FINISHED DANS LA DB
      //PATTERN:tasks/toggleFinish/id
      //DONNEES: -finished:la propriété checked transformée (0 ou 1)
      //RETURN: 0(mal passé) ou 1(bien passé)
    $.ajax({
        url: 'tasks/toggleFinish/'+id,
        method: 'POST',
        data: {
          finished:toggle
        },
        success: (reponsePHP)=>{
          if (reponsePHP!=0) {
            $(this).closest('li').toggleClass('completed');
            compteurTaches();
          }
        },
        error: function(){
          window.alert("Problème durant la transaction...");
        }
    });
  });
  /*-----------------------------------FIN POSSIBILITE DE COCHER UNE TACHE POUR LA TERMINER-----------------------------------*/

  /*-----------------------------------------RENDRE UNE TACHE NON TERMINEE MODIFIABLE-----------------------------------------*/

  /*TRANSFORMATION EN INPUT*/
  $('.todo-list').on('dblclick', '.todo:not(.completed) label', function(){
    var contenu=$(this).text();
    $(this).html('<input type="text" />').find('input').val(contenu).focus();
  })

  /*EN CAS DE MODIFICATION DE L'INPUT*/
  $('.todo-list').on('change', '.todo:not(.completed) label input', function(){
    var contenu=$(this).val();
    var id=$(this).closest('li').attr('data-id');
    // AJAX - MODIFICATION D'UNE TACHE NON-TERMINEE DANS LA DB
      //PATTERN:tasks/edit/id
      //DONNEES: -content:valeur de l'input
      //RETURN: 0(si mal passé), 1(si bien passé)
    $.ajax({
        url: 'tasks/edit/'+id,
        method: 'POST',
        data: {
          content:contenu
        },
        success: (reponsePHP)=>{
          if (reponsePHP==0) {
            alert('erreur lors de la modification, veuillez actualiser la page!')
          }
        },
        error: function(){
          window.alert("Problème durant la transaction...");
        }
    });
  });

  /*RETOUR A L'ORIGINE*/
    /*SI PERTE DE FOCUS*/
    $('.todo-list').on('blur', '.todo:not(.completed) label input', function(){
      var contenu=$(this).val();
      $(this).closest('label').text(contenu);
    });
    /*SI ON APPUIE SUR ENTER*/
    $('.todo-list').on('keyup','.todo:not(.completed) label input', function(e){
      if(e.keyCode==13){
        $(this).blur();
      }
    });
  /*--------------------------------------FIN RENDRE MODIFIABLE UNE TACHE NON-TERMINEE--------------------------------------*/

  /*---------------------------------------------------------FILTRES---------------------------------------------------------*/
  /*BOUTON QUI AFFICHE TOUTES LES TACHES*/
  $('.filters .all').click(function(e){
    e.preventDefault();
    $('.todo-list li:not(.completed)').slideDown();
    $('.todo-list li.completed').slideDown();
  });
  /*BOUTON QUI AFFICHE UNIQUEMENT LES TERMINES*/
  $('.filters .completed').click(function(e){
    e.preventDefault();
    $('.todo-list li:not(.completed)').slideUp();
    $('.todo-list li.completed').slideDown();
  });
  /*BOUTON QUI AFFICHE UNIQUEMENT LES NON-TERMINES*/
  $('.filters .active').click(function(e){
    e.preventDefault();
    $('.todo-list li:not(.completed)').slideDown();
    $('.todo-list li.completed').slideUp();
  });
  /*---------------------------------------------------------FIN FILTRES---------------------------------------------------------*/

  /*--------------------------------------------------SUPPRIMER LES TERMINES--------------------------------------------------*/

  $('.clear-completed').click(function(e){
    e.preventDefault();
    // AJAX - SUPPRESSION DES TACHES TERMINEES
      //PATTERN: tasks/deleteFinished
      //DONNEES: /
      //RETURN: 0(si mal passé), 1(si bien passé)
    $.ajax({
        url: 'tasks/deleteFinished',
        success: function(reponsePHP){
          if (reponsePHP!=0) {
            $('.todo-list li.completed').slideUp(function(){
              $(this).remove();
              compteurTaches();
            });
          }
        },
        error: function(){
          window.alert("Problème durant la transaction...");
        }
    });
  })

  /*--------------------------------------------------FIN SUPPRIMER LES TERMINES--------------------------------------------------*/

});
